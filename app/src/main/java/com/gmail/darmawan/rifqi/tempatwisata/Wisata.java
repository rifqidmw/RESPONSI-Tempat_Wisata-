package com.gmail.darmawan.rifqi.tempatwisata;

/**
 * Created by yolo on 28/08/18.
 */

public class Wisata {
    private int id;
    private String nama;
    private String alamat;
    private byte[] image;

    public Wisata(int id, String nama, String alamat, byte[] image) {
        this.id = id;
        this.nama = nama;
        this.alamat = alamat;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
