package com.gmail.darmawan.rifqi.tempatwisata;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.GridView;

import java.util.ArrayList;

public class ListWisataActivity extends AppCompatActivity {

    GridView gridView;
    ArrayList<Wisata> list;
    WisataAdapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_wisata);

        gridView = (GridView) findViewById(R.id.gridView);
        list = new ArrayList<>();
        adapter = new WisataAdapter(this, R.layout.wisata_item, list);
        gridView.setAdapter(adapter);

        Cursor cursor = MainActivity.sqLiteHelper.getData("SELECT * FROM WISATA");
        list.clear();
        while (cursor.moveToNext()){
            int id = cursor.getInt(0);
            String nama = cursor.getString(1);
            String alamat = cursor.getString(2);
            byte[] image = cursor.getBlob(3);

            list.add(new Wisata(id, nama, alamat, image));
        }
        adapter.notifyDataSetChanged();
    }
}
